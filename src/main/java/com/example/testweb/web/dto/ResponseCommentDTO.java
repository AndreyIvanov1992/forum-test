package com.example.testweb.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@ApiModel("Комментарий")
@Builder
@Getter
public class ResponseCommentDTO {

    @ApiModelProperty("Уникальный идентификатор комментария")
    private final Long id;

    @ApiModelProperty("Текст комментария")
    private final String text;

    @ApiModelProperty("e-mail автора комментария")
    private final String authorEmail;

    @ApiModelProperty("Идентификатор темы, к которой относиться комментарий")
    private final Long subjectId;
}
