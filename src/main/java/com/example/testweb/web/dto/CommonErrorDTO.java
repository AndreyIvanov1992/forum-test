package com.example.testweb.web.dto;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.time.Instant;
import java.util.List;

@ApiModel("Обычная ошибка")
@Data
@Builder
@AllArgsConstructor
public class CommonErrorDTO {

    private int status;

    private String code;

    private List<String> messages;

    private Instant timestamp;

    private String stacktrace;
}
