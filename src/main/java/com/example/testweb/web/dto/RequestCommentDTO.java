package com.example.testweb.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ApiModel("Комментарий")
@AllArgsConstructor
@Getter
public class RequestCommentDTO {

    @ApiModelProperty("Текст комментария")
    private final String text;

    @ApiModelProperty("e-mail автора комментария")
    private final String authorEmail;

    @ApiModelProperty("Идентификатор темы, к которой относиться комментарий")
    private final Long subjectId;
}
