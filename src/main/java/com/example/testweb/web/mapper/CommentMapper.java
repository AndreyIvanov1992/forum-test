package com.example.testweb.web.mapper;

import com.example.testweb.domain.model.Comment;
import com.example.testweb.web.dto.RequestCommentDTO;
import com.example.testweb.web.dto.ResponseCommentDTO;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper {

    public Comment convertRequestCommentDTOToComment(RequestCommentDTO requestCommentDto) {
        return Comment
                .builder()
                .authorEmail(requestCommentDto.getAuthorEmail())
                .subjectId(requestCommentDto.getSubjectId())
                .text(requestCommentDto.getText())
                .build();
    }

    public ResponseCommentDTO convertCommentToResponseCommentDTO(Comment comment) {
        return ResponseCommentDTO
                .builder()
                .id(comment.getId())
                .authorEmail(comment.getAuthorEmail())
                .subjectId(comment.getSubjectId())
                .text(comment.getText())
                .build();
    }
}
