package com.example.testweb.web.controller;

import com.example.testweb.domain.model.Comment;
import com.example.testweb.domain.service.CommentService;
import com.example.testweb.domain.service.PutResult;
import com.example.testweb.web.dto.RequestCommentDTO;
import com.example.testweb.web.dto.ResponseCommentDTO;
import com.example.testweb.web.mapper.CommentMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;


import java.util.List;
import java.util.stream.Collectors;


@Api("Комментарии")
@RestController
@AllArgsConstructor
@RequestMapping("/comments")
public class CommentController {

    private final CommentService commentService;

    private final CommentMapper commentMapper;

    @ApiOperation("Получение списка комментариев")
    @GetMapping
    public List<ResponseCommentDTO> getAllComments(@ApiParam("Идентификатор темы") @RequestParam(required = false) Long subjectId) {
        List<Comment> comments = subjectId == null ? commentService.getAllComments() : commentService.getAllCommentsBySubject(subjectId);
        return comments.stream().map(commentMapper::convertCommentToResponseCommentDTO).collect(Collectors.toList());
    }

    @ApiOperation("Получение комментария")
    @GetMapping("/{id}")
    public ResponseCommentDTO getComment(@ApiParam("Идентификатор комментария") @PathVariable Long id) {
        return commentMapper.convertCommentToResponseCommentDTO(commentService.getCommentById(id));
    }

    @ApiOperation(value = "Добавление нового комментария")
    @PostMapping(produces = "application/json")
    public ResponseEntity<Void> postComment(
            UriComponentsBuilder uriComponentsBuilder,
            @RequestBody RequestCommentDTO commentDTO) {
        long newId = commentService.addNewComment(commentMapper.convertRequestCommentDTOToComment(commentDTO));
        return getCreatedCommentResponseEntity(uriComponentsBuilder, newId);
    }

    @ApiOperation("Добавление коментария по ID")
    @PutMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Void> putComment(
            UriComponentsBuilder uriComponentsBuilder,
            @ApiParam("Идентификатор комментария") @PathVariable Long id,
            @RequestBody RequestCommentDTO commentDTO) {
        PutResult putResult = commentService.putComment(id, commentMapper.convertRequestCommentDTOToComment(commentDTO));
        if (putResult == PutResult.CREATED) {
            return getCreatedCommentResponseEntity(uriComponentsBuilder, id);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @ApiOperation("Удаление коментария")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteComment(@ApiParam("Идентификатор комментария") @PathVariable Long id) {
        commentService.removeComment(id);
        return ResponseEntity.noContent().build();
    }

    private ResponseEntity<Void> getCreatedCommentResponseEntity(UriComponentsBuilder uriComponentsBuilder, long commentId) {
        UriComponents uriComponents =
                uriComponentsBuilder.path("comments/{id}").buildAndExpand(commentId);
        return ResponseEntity
                .created(uriComponents.toUri())
                .build();
    }

}
