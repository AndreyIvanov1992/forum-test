package com.example.testweb.web.controller;

import com.example.testweb.domain.exception.CheckCommentException;
import com.example.testweb.domain.exception.CommentNotFoundException;
import com.example.testweb.web.dto.CommonErrorDTO;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class CommonExceptionHandler {

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<CommonErrorDTO> handleException(Exception e) {
        ResponseEntity<CommonErrorDTO> result;
        if (e instanceof CommentNotFoundException) {
            result = process(e, HttpStatus.NOT_FOUND);
        } else if (e instanceof CheckCommentException) {
            result = process(e, HttpStatus.BAD_REQUEST, ((CheckCommentException) e).getErrors());
        } else {
            result = process(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return result;
    }

    private ResponseEntity<CommonErrorDTO> process(Exception exception, HttpStatus httpStatus) {

        String errorMessage = exception.getMessage() == null ? exception.getClass().getName() : exception.getMessage();
        return process(exception, httpStatus, Collections.singletonList(errorMessage));
    }

    private ResponseEntity<CommonErrorDTO> process(Exception exception, HttpStatus httpStatus, List<String> messages) {
        CommonErrorDTO commonErrorDTO = CommonErrorDTO
                .builder()
                .status(httpStatus.value())
                .code(httpStatus.name())
                .messages(messages)
                .stacktrace(ExceptionUtils.getStackTrace(exception))
                .timestamp(Instant.now())
                .build();
        return ResponseEntity
                .status(httpStatus.value())
                .body(commonErrorDTO);
    }
}
