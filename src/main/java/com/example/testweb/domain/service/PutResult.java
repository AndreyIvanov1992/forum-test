package com.example.testweb.domain.service;

public enum PutResult {
    CREATED,
    UPDATED
}
