package com.example.testweb.domain.service;

import com.example.testweb.domain.exception.CheckCommentException;
import com.example.testweb.domain.exception.CommentNotFoundException;
import com.example.testweb.domain.model.Comment;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class CommentService {

    private static final Map<Long, Comment> comments = new ConcurrentHashMap<>();

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    static {
        comments.put(1L, new Comment(1L, "Ты лох", "pirogki1@gmail.com", 333L));
        comments.put(2L, new Comment(2L, "Сам ты лох", "1992.ivanov.andrey@gmail.com", 333L));
        comments.put(3L, new Comment(3L, "Да вы все лохи", "Andrey_iww@mail.ru", 333L));
    }

    public List<Comment> getAllCommentsBySubject(long subjectId) {
        return comments
                .values()
                .stream()
                .filter(comment -> comment.getSubjectId() == subjectId)
                .collect(Collectors.toList());
    }

    public List<Comment> getAllComments() {
        return new ArrayList<>(comments.values());
    }

    public Comment getCommentById(Long id) throws CommentNotFoundException {
        Comment comment = comments.get(id);
        if (comment == null) {
            throw new CommentNotFoundException();
        }
        return comment;
    }

    public long addNewComment(Comment comment) throws CheckCommentException {
        checkComment(comment);
        long id = comments.keySet().stream().max(Long::compareTo).orElse(0L) + 1;
        comment.setId(id);
        comments.put(id, comment);
        return id;
    }

    public PutResult putComment(long id, Comment comment) {
        PutResult putResult = comments.containsKey(id) ? PutResult.UPDATED : PutResult.CREATED;
        comment.setId(id);
        comments.put(id, comment);
        return putResult;
    }

    public void removeComment(long id) {
        if(!comments.containsKey(id))  {
            throw new CommentNotFoundException();
        }
        comments.remove(id);
    }

    private void checkComment(Comment comment) throws CheckCommentException {

        List<String> errorMessages = new ArrayList<>();
        if (comment.getText() == null || comment.getText().isEmpty()) {
            errorMessages.add("Comment cannot be empty");
        }

        if (comment.getAuthorEmail() == null || comment.getAuthorEmail().isEmpty()) {
            errorMessages.add("Email cannot be empty");
        }

        if (comment.getSubjectId() == null) {
            errorMessages.add("SubjectId cannot be empty");
        }

        if(comment.getAuthorEmail() != null) {
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(comment.getAuthorEmail());
            if (!matcher.find()) {
                errorMessages.add("Email is not valid");
            }
        }
        if (!errorMessages.isEmpty()) {
            throw new CheckCommentException(errorMessages);
        }
    }

}
