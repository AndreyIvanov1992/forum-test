package com.example.testweb.domain.exception;

import lombok.Getter;

import java.util.List;

public class CheckCommentException extends RuntimeException {

    @Getter
    private final List<String> errors;

    public CheckCommentException(List<String> errors) {
        super("Comment is not valid");
        this.errors = errors;
    }
}
