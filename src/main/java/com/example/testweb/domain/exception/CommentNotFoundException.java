package com.example.testweb.domain.exception;


public class CommentNotFoundException extends RuntimeException  {
    public CommentNotFoundException() {
        super("Comment not found");
    }
}
