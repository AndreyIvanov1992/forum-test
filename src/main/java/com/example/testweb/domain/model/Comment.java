package com.example.testweb.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Comment {

    private Long id;

    private String text;

    private String authorEmail;

    private Long subjectId;
}
